package sector

import "errors"

const doesntExist = "attribute_not_exists('p')"

var ErrDynamoDbUnmarshal = errors.New("error unmarshalling dynamo db value")

type prv string

const (
	PrvOpen     = "o"
	PrvPassword = "p"
	// PrvFriends  prv = "f"
	// PrvInvite   prv = "i"
)

const (
	indexSectorsSort       = "SectorsSort"
	indexSectorConnections = "SectorConnections"
	indexUserConnection    = "UserConnection"
)
