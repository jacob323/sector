package sector_test

import (
	"errors"
	"fmt"
	"os"
	"testing"

	. "bitbucket.org/jacob323/errorclass"

	. "bitbucket.org/jacob323/sector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

var (
	db DB
)

func init() {
	db = DB{
		T: dynamo.New(session.New(), &aws.Config{
			Region:      aws.String("us-east-2"),
			Credentials: credentials.NewStaticCredentials(os.Getenv("CX_ID"), os.Getenv("CX_SECRET"), ""),
		}).Table("crux"),
	}
}

type TSector struct {
	Sector
	TData string
}
type TSectorData struct {
	DataEntry
	TData string
}

var (
	s1 = &TSector{
		Sector: Sector{SectorPK("s1"), SectorSK, PrvOpen, ""},
		TData:  "test data",
	}
	s1d1 = &TSectorData{
		DataEntry{PK: SectorPK("s1"), SK: DataEntrySK("key1")},
		"test data",
	}
	s1d2 = &TSectorData{
		DataEntry{PK: SectorPK("s1"), SK: DataEntrySK("key1")},
		"test data 2",
	}
	s1d3 = &TSectorData{
		DataEntry{PK: SectorPK("s1"), SK: DataEntrySK("key1")},
		"test data 3",
	}
)

func rmvS1(logError bool) {
	err := db.RemoveSector(s1)
	if logError && err != nil {
		fmt.Println("Cleanup error: ", err)
	}
}

func TestAddNewSector(t *testing.T) {
	rmvS1(false)
	t.Cleanup(func() { rmvS1(true) })
	err := db.AddSector(s1)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	err = db.AddSector(s1)
	if !errors.Is(err, ErrExists{}) {
		t.Error(err)
		t.FailNow()
	}
	s2 := &TSector{}
	s2.PK = SectorPK("s1")
	s2.SK = SectorSK
	err = db.GetSector(s2)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if s1.TData != s2.TData {
		t.Errorf("TData from Get is '%s' but TData should be '%s'", s2.TData, s1.TData)
	}
}

func TestAddData(t *testing.T) {
	t.Cleanup(func() {
		err := db.RemoveSector(s1)
		if err != nil {
			fmt.Println("Cleanup error: ", err)
		}
	})
	err := db.AddSector(s1)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	err = db.AddDataEntry(s1d1, nil, 3)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	err = db.AddDataEntry(s1d2, nil, 3)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	err = db.AddDataEntry(s1d3, nil, 3)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	s1d := []TSectorData{}
	err = db.GetDataEntriesAll(s1d1.PK.String(), "key1", &s1d)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if len(s1d) != 3 {
		t.Error("GetDataALl returned wrong count")
		t.FailNow()
	}
	if s1d[0].TData != "test data" {
		t.Errorf("TData from AddData is '%s' but should be '%s'", s1d[0].TData, s1d1.TData)
	}
}

func TestRemoveSector(t *testing.T) {
	// test sector with < 25 rows
	// test sector with 25 rows
	// test sector with > 25 rows
	// t.Error("test NYI")
}
