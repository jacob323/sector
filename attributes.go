package sector

import "time"

type attribute interface {
	pk() string
	sk() string
}

type Expiration struct {
	Time time.Time `dynamo:"exp,unixtime,omitempty"`
}

func (e *Expiration) Lives(hours int) {
	e.Time = time.Now().Add(time.Duration(hours) * time.Hour)
}

// Sector
type Sector struct {
	PK  compoundKey `dynamo:"p,hash" json:"p"`
	SK  string      `dynamo:"s,range" json:"s"`
	Prv prv
	PW  string `dynamo:",omitempty" json:"-"`
}
type sector interface{ sector() *Sector }

func (s *Sector) sector() *Sector { return s }

// Data
type Data struct {
	PK compoundKey `dynamo:"p,hash" json:"p"`
	SK compoundKey `dynamo:"s,range" json:"s"`
}
type data interface{ data() *Data }

func (d *Data) data() *Data { return d }

// DataEntry
type DataEntry struct {
	PK compoundKey `dynamo:"p,hash" json:"p"`
	SK countingKey `dynamo:"s,range" json:"s"`
}
type dataEntry interface{ dataEntry() *DataEntry }

func (d *DataEntry) dataEntry() *DataEntry { return d }

// SectorConnection
type SectorConnection struct {
	PK compoundKey `dynamo:"p,hash" json:"p"`
	SK compoundKey `dynamo:"s,range" json:"s"`
	// SC is the connection ID
	SC string // TODO
	U  string
	Expiration
}
type sectorConnection interface{ sectorConnection() *SectorConnection }

func (s *SectorConnection) sectorConnection() *SectorConnection { return s }

// User
type User struct {
	PK compoundKey `dynamo:"p,hash" json:"p"`
	SK string      `dynamo:"s,range" json:"s"`
}
type user interface{ user() *User }

func (u *User) user() *User { return u }

// UserConnection
type UserConnection struct {
	PK compoundKey `dynamo:"p,hash" json:"p"`
	SK string      `dynamo:"s,range" json:"s"`
	// UC is the connection ID
	UC string
	Expiration
}
type userConnection interface{ userConnection() *UserConnection }

func (u *UserConnection) userConnection() *UserConnection { return u }
