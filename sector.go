package sector

import (
	"errors"
	"fmt"
	"strings"

	. "bitbucket.org/jacob323/errorclass"

	dysdk "github.com/aws/aws-sdk-go/service/dynamodb"
	dy "github.com/guregu/dynamo"
)

// https://docs.google.com/spreadsheets/d/19Bzv4K5bDtLI5X5xcznixweWx5-BXjJVW5v0pYbGb60/edit#gid=0

type DB struct{ T dy.Table } // TODO: change name to more generic and add apig ws stuff? Or should DB be lazy?

func (db DB) GetSector(out sector) error {
	return db.T.Get("p", out.sector().PK).
		Range("s", dy.Equal, SectorSK).
		One(out)
}

func (db DB) GetDataEntryFinal(key string, out dataEntry) error {
	return db.T.Get("p", out.dataEntry().PK).
		Range("s", dy.BeginsWith, DataSK(key).String()).
		Order(dy.Descending).
		Limit(1).
		One(out)
}

func (db DB) GetDataEntriesAll(pk string, key string, out interface{}) error {
	return db.T.Get("p", pk).
		Range("s", dy.BeginsWith, fmt.Sprint("##", key, "#")).
		All(out)
}

// AddData `d` after `after`. If `after` is nil, query DB to obtain.
func (db DB) AddDataEntry(d dataEntry, after dataEntry, retry int) error {
	var err error
	sk := d.dataEntry().SK
	i := -1
	if after != nil {
		i = after.dataEntry().SK.int + 1
	} else {
		after = &DataEntry{PK: d.dataEntry().PK}
		err = db.GetDataEntryFinal(sk.string, after)
		if errors.Is(err, dy.ErrNotFound) {
			i = 0
		} else if err != nil {
			return ErrTODO{err}
		} else {
			i = after.dataEntry().SK.int + 1
		}
	}
	for stop := i + retry; i <= stop; i++ {
		d.dataEntry().SK.int = i
		err = db.T.Put(d).If(doesntExist).Run()
		if err == nil {
			return nil
		}
		if errors.As(err, ptr(&dysdk.ConditionalCheckFailedException{})) {
			continue
		}
		return ErrUnexpected{err}
	}
	return ErrExhaustedRetries{err}
}

func (db DB) AddUser(u user) error {
	return db.add(u)
}

func (db DB) AddSector(s sector) error {
	return db.add(s)
}

func (db DB) add(v interface{}) error {
	err := db.T.Put(v).If(doesntExist).Run()
	if err == nil {
		return nil
	}
	if errors.As(err, ptr(&dysdk.ConditionalCheckFailedException{})) {
		return ErrExists{}
	}
	return ErrTODO{err}
}

func (db DB) RemoveSector(s sector) error {
	return db.deleteWithPK(s.sector().PK.String())
}

func (db DB) deleteWithPK(pk string) error {
	iter := db.T.Get("p", pk).Iter()
	key := justTheKey{}
	var err error
	for {
		batch := db.T.Batch("p", "s").Write()
		for i := 0; i < 25; i++ { // dynamodb limit of 25 ops
			if !iter.Next(&key) {
				if err = iter.Err(); err != nil {
					return ErrTODO{err}
				}
				if i == 0 {
					return nil
				}
				break
			}
			batch.Delete(dy.Keys{pk, key.SK})
		}
		wrote, err := batch.Run()
		if err != nil {
			return ErrTODO{err}
		}
		if wrote != 25 {
			break
		}
	}
	return nil
}

func (db DB) Leave(sector string, connId string) error {
	return ErrTODO{}
}
func (db DB) Join(sector string, connId string, username string) error {
	return ErrTODO{}
}
func (db DB) Move(sectorPK compoundKey, connId string) error {
	scs := []SectorConnection{}
	err := db.T.Get("SC", connId).Index(indexSectorConnections).All(&scs)
	if err != nil {
		return ErrTODO{}
	}
	var un string
	for _, sc := range scs { // TODO consider batching
		un = sc.U
		err = db.T.Delete("p", sc.PK.String()).Range("s", sc.SK.String()).Run()
		if err != nil {
			return ErrTODO{}
		}
	}
	if un == "" {
		uc := UserConnection{}
		err := db.T.Get("UC", connId).Index(indexUserConnection).One(&uc)
		if err != nil {
			return ErrTODO{}
		}
		un = uc.PK.string
	}
	sc := &SectorConnection{PK: sectorPK, SK: SectorConnectionSK(connId), SC: connId, U: un}
	sc.Lives(40)
	err = db.T.Put(sc).Run()
	if err != nil {
		return ErrTODO{err}
	}
	return nil
}

func (db DB) GetSectors(out interface{}, publicOpt publicOpt, prefix ...string) error {
	if publicOpt == Any {
		return ErrTODO{}
	}
	prefixStr := strings.Join(
		append(append(make([]string, 0, len(prefix)+1), "s"), prefix...),
		"#",
	)
	if publicOpt.pub {
		err := db.T.Get("Prv", PrvOpen).
			Range("p", dy.BeginsWith, prefixStr).
			Index(indexSectorsSort).
			All(out)
		if err != nil {
			return err
		}
	}
	if publicOpt.priv {
		err := db.T.Get("Prv", PrvPassword).
			Range("p", dy.BeginsWith, prefixStr).
			Index(indexSectorsSort).
			All(out)
		if err != nil {
			return err
		}
	}
	return nil
}

type publicOpt struct{ pub, priv bool }

var (
	OnlyPublic  = publicOpt{true, false}
	OnlyPrivate = publicOpt{false, true}
	Any         = publicOpt{true, true}
)

func ptr(v interface{}) *interface{} {
	return &v
}
