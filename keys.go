package sector

import (
	"fmt"
	"strconv"

	dysdk "github.com/aws/aws-sdk-go/service/dynamodb"
	dy "github.com/guregu/dynamo"
)

type key interface {
	fmt.Stringer
	dy.Unmarshaler
	dy.Marshaler
}

type compoundKey struct {
	rune
	string
}

func (k compoundKey) String() string {
	return fmt.Sprintf("%c#%s", k.rune, k.string)
}
func (k compoundKey) MarshalDynamo() (*dysdk.AttributeValue, error) {
	v := &dysdk.AttributeValue{}
	v.SetS(k.String())
	return v, nil
}
func (s compoundKey) UnmarshalDynamo(v *dysdk.AttributeValue) error {
	if v.S == nil || len(*v.S) < 3 {
		return ErrDynamoDbUnmarshal
	}
	s.rune = rune((*v.S)[0])
	s.string = (*v.S)[2:]
	return nil
}

type countingKey struct {
	string
	int
}

func (k countingKey) String() string {
	i := k.int
	if i < 0 || i > 9999 {
		i = 9999
	}
	return fmt.Sprintf("##%s#%04d", k.string, i)
}
func (k countingKey) MarshalDynamo() (*dysdk.AttributeValue, error) {
	v := &dysdk.AttributeValue{}
	v.SetS(k.String())
	return v, nil
}
func (s countingKey) UnmarshalDynamo(v *dysdk.AttributeValue) error {
	if v.S == nil || *v.S == "" {
		return ErrDynamoDbUnmarshal
	}
	s.string = (*v.S)[2:]
	if len(s.string) < 6 {
		return ErrDynamoDbUnmarshal
	}
	var err error
	s.int, err = strconv.Atoi(s.string[len(s.string)-4:])
	if err != nil {
		return ErrDynamoDbUnmarshal
	}
	s.string = s.string[:len(s.string)-5] // todo

	return nil
}

func SectorPK(path string) compoundKey {
	return compoundKey{'s', path}
}

const SectorSK = "#"

func UserPK(username string) compoundKey {
	return compoundKey{'u', username}
}

const UserSK = "#"

func SectorConnectionSK(connid string) compoundKey {
	return compoundKey{'c', connid}
}

const UserConnectionSK = "c"

func DataSK(key string) compoundKey {
	return compoundKey{'#', key}
}

func DataEntrySK(key string) countingKey {
	return countingKey{key, 0}
}

// func DataEntrySK(key string, index int) countingKey {
// 	return countingKey{key, index}
// }

type justTheKey struct {
	PK string `dynamo:"p,hash" json:"p"`
	SK string `dynamo:"s,range" json:"s"`
}
