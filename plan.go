package sector

var _ interface {
	// Put(attribute) error
} = DB{}

var (
	_ key = compoundKey{}
	_ key = countingKey{}
)

var (
	_ sector    = &Sector{}
	_ data      = &Data{}
	_ dataEntry = &DataEntry{}
)
