module bitbucket.org/jacob323/sector

go 1.16

require (
	bitbucket.org/jacob323/errorclass v0.0.0-20210625200026-7d7f02ec5750
	github.com/aws/aws-sdk-go v1.38.54
	github.com/guregu/dynamo v1.10.4
)
